import {} from 'jasmine';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { Logger } from '../core/logger/logger.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TrainingDetailComponent } from './training-detail.component';
import { TrainingDataComponent } from '../shared/forms/training-data/training-data.component';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';
import { TrainingModel } from '../models/training-model';
import { TrainingService } from '../services/training.service';
import { MentorService } from '../services/mentor.service';
import { TechnologyService } from '../services/technology.service';
import { MentorModel } from '../models/mentor-model';
import { TechnologyModel } from '../models/technology-model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule } from '@angular/material/radio';
import {MatInputModule} from '@angular/material';
describe('TrainingDetailComponent', () => {
  let component: TrainingDetailComponent;
  let fixture: ComponentFixture<TrainingDetailComponent>;
  
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingDetailComponent,TrainingDataComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,
               RouterTestingModule,
               HttpClientTestingModule,
               MatSnackBarModule,
               NgxMatDatetimePickerModule,
               NgxMatTimepickerModule,
               NgxMatNativeDateModule,
               MatSelectModule,
               MatRadioModule,
               MatInputModule,
               BrowserAnimationsModule],
      providers:[TrainingService,TechnologyService,MentorService,{ provide: AppConfigService, useValue: mockedConfigService},{ provide: MAT_DIALOG_DATA, useValue: {} },{ provide: MatDialogRef, useValue: {} }]
    })
    .compileComponents();
    configService = TestBed.get(AppConfigService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

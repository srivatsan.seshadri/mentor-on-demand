import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { TrainingModel } from '../models/training-model';
import { TrainingDataComponent } from '../shared/forms/training-data/training-data.component';
import { TrainingService } from '../services/training.service';
import { MentorService } from '../services/mentor.service';
import { TechnologyService } from '../services/technology.service';
import { MentorModel } from '../models/mentor-model';
import { TechnologyModel } from '../models/technology-model';

@Component({
  selector: 'app-training-detail',
  templateUrl: './training-detail.component.html',
  styleUrls: ['./training-detail.component.scss']
})
export class TrainingDetailComponent implements OnInit {
  title = 'Training Technology';
  form: FormGroup;
  technologies: TechnologyModel[];
  mentors: MentorModel[];
  constructor(private trainingService: TrainingService,
              private mentorService: MentorService,
              private techService: TechnologyService,
        private dialogRef: MatDialogRef<TrainingDetailComponent>,
        @Inject(MAT_DIALOG_DATA) public data: TrainingModel) { }

  ngOnInit() {
    this.buildForm();
   this.loadTechnologies();
    this.loadMentors(); 
  }

  buildForm() {
        this.form = new FormGroup({
            id: new FormControl(this.data.id),
            mentorId: new FormControl(this.data.mentorId),
            skillId: new FormControl(this.data.skillId),
            status: new FormControl(this.data.status),
            progress: new FormControl(this.data.progress),
            rating: new FormControl(this.data.rating),
            startDate: new FormControl(this.data.startDate),
            endDate: new FormControl(this.data.endDate),
            fees: new FormControl(this.data.fees),
        });
    }

    save() {
        if (this.form.invalid) {
            return;
        }
        this.dialogRef.close(this.form.value);
    }

    close() {
        this.dialogRef.close(null);
    }

  loadTechnologies() {
    this.techService.getTechnologies().subscribe(data => {
      this.technologies = data;
      console.log(this.technologies);
    });
  }

  loadMentors() {
    this.mentorService.getMentors().subscribe(data => {
      this.mentors = data;
      console.log(this.mentors);
    });
  }
}

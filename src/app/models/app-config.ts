export class AppConfig {
  authApiEndpoint: string;
  payApiEndpoint: string;
  searchApiEndpoint: string;
  techApiEndpoint: string;
  trainingApiEndpoint: string;
  userauthApiEndpoint: string;
  searchauthApiEndpoint: string;
  proposalauthApiEndpoint: string;
}
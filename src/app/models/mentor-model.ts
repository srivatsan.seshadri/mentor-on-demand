import { Moment } from 'moment';
import { RoleModel } from './role-model';
export class MentorModel {
    id: number;
    userName: string;
    password: string;
    firstName: string;
    lastName: string;
    contactNumber: string;
    regDateTime: Moment;
    regCode: string;
    forceResetPassword: boolean;
    active: boolean;
    roleId: number;
    role: RoleModel;
    lastLoginDate: Moment;
}

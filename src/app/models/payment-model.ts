import {Moment} from 'moment';
import {UserModel} from './user-model';
import {TrainingModel} from './training-model';
export class PaymentModel {
    id:number;
    mentor:UserModel;
    mentorId:number;
    training:TrainingModel;
    trainingId:number;
    txnType:string;
    amount:number;
    paymentDate:Moment;
    remarks:string;
}

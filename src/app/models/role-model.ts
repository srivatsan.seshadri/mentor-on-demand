import {UserModel} from './user-model';
export class RoleModel {
    roleId:number;
    rolename:string;
    roleDesc:string;
    users:UserModel[]
}

export class TechnologyModel {
    id:number;
    name:string;
    termsOfConditions:string
    duration:number;
    prerequisites:string;
}

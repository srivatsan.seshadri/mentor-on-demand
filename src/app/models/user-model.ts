import {Moment} from 'moment'
import {RoleModel} from './role-model'
import {MentorSkillsModel} from './mentor-skills-model'
export class UserModel {
id:number;
userName:string;
password:string;
firstName:string;
lastName:string;
contactNumber:string;
yearsOfExperience:number;
linkedInUrl:string;
mentorSkills:MentorSkillsModel[];
regDateTime :Moment;
regCode:string;
forceResetPassword:boolean;
active:boolean;
roleId:number;
role:RoleModel;
lastLoginDate:Moment;
}

import {Moment} from 'moment'
import {UserModel} from './user-model'
import {PaymentModel} from './payment-model'
import {TechnologyModel} from './technology-model'
export class TrainingModel {
    id:number;
    mentorId:number;
    mentor:UserModel;
    paymentId:number;
    payment:PaymentModel;
    userId:number;
    skill:TechnologyModel;
    skillId:number;
    status:boolean;
    progress:number;
    rating:number;
    startDate:Moment;
    endDate:Moment;
    amountReceived:number;
    trainingId:number;
    fees:number;
}

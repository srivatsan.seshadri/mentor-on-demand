export class SearchResultModel {
    id:number
    firstName:string;
    lastName:string;
    rating:string;
    yearsOfExperience:number;
}

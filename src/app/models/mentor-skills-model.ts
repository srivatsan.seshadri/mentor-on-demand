import {TechnologyModel} from './technology-model'
export class MentorSkillsModel {
    id:number;
    mentorId:number;
    skillId:number;
    skillName:string;
    skill:TechnologyModel;
    selfRating:number;
    yearsOfExperience:number;
    trainingsDelivered:number;
    facilitiesOffered:string;
}

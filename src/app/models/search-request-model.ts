import { Moment } from 'moment';
export class SearchRequestModel {
     technologyId:number;
     startDate:Moment;
     endDate:Moment;
}

import {Moment} from 'moment';
export class TrainingListModel {

    id:number;
    mentorId:number;
    mentorName:string;
    skill:string;
    skillId:number;
    status:boolean;
    progress:number;
    rating:number;
    startDate:Moment;
    endDate:Moment;
    amountReceived:number;
    trainingId:number;
    fees:number;
}

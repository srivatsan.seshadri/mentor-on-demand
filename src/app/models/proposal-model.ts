import { Moment } from 'moment';
export class ProposalModel {
    id: number;
    mentorId: number;
    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
    proposalDate: Moment;
    isApproved: boolean;
}

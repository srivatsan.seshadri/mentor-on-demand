import {Moment} from 'moment';
export class PaymentListModel {
    id:number;
    mentorFirstName:string;
    mentorLastName:string;
    mentorId:string;
    training:string;
    TrainingId:number;
    txnType:string;
    Amount:number;
    paymentDate:Moment;
    remarks:string;
}

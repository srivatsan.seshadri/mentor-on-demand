import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UserListComponent } from './user-list/user-list.component';
import { TechnologyListComponent } from './technology-list/technology-list.component'
import { PaymentListComponent } from './payment-list/payment-list.component'
import { NewUserComponent } from './new-user/new-user.component'
import { NewTechComponent } from './new-tech/new-tech.component';
import { TrainingListComponent } from './training-list/training-list.component';
import { NewTrainingComponent } from './new-training/new-training.component';
import { NewPaymentComponent } from './new-payment/new-payment.component'
import {SearchMentorComponent} from './search-mentor/search-mentor.component';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
const routes: Routes = [{
	path: '',
	component: LoginComponent
}
	, {
	path: 'home',
	component: HomeComponent
}
	, {
	path: 'users',
	component: UserListComponent
}, {
	path: 'add-user',
	component: NewUserComponent
}, {
	path: 'technologies',
	component: TechnologyListComponent
}, {
	path: 'add-technology',
	component: NewTechComponent
}, {
	path: 'trainings',
	component: TrainingListComponent
}, {
	path: 'add-training',
	component: NewTrainingComponent
},{
	path: 'payments',
	component: PaymentListComponent
},{
	path: 'add-payment',
	component: NewPaymentComponent
},
{
	path:'search-mentor',
	component:SearchMentorComponent
}];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }

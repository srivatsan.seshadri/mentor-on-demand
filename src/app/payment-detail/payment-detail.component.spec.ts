import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {} from 'jasmine';
import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { Logger } from '../core/logger/logger.service';

import { PaymentDetailComponent } from './payment-detail.component';

import { PaymentModel } from '../models/payment-model';
import { PaymentDataComponent } from '../shared/forms/payment-data/payment-data.component';
import { PaymentService } from '../services/payment.service';
import { MentorService } from '../services/mentor.service';
import { TrainingService } from '../services/training.service';
import { TrainingModel } from '../models/training-model';
import { TrainingListModel } from '../models/training-list-model';
import { UserModel } from '../models/user-model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

describe('PaymentDetailComponent', () => {
  let component: PaymentDetailComponent;
  let fixture: ComponentFixture<PaymentDetailComponent>;
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentDetailComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,RouterTestingModule,HttpClientTestingModule,MatSnackBarModule],
      providers:[TrainingService,PaymentService,MentorService,{ provide: AppConfigService, useValue: mockedConfigService},{ provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }]
    })
    .compileComponents();
    configService = TestBed.get(AppConfigService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

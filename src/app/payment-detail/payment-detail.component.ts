import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { PaymentModel } from '../models/payment-model';
import { PaymentDataComponent } from '../shared/forms/payment-data/payment-data.component';
import { PaymentService } from '../services/payment.service';
import { MentorService } from '../services/mentor.service';
import { TrainingService } from '../services/training.service';
import { TrainingModel } from '../models/training-model';
import { TrainingListModel } from '../models/training-list-model';
import { UserModel } from '../models/user-model';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.scss']
})
export class PaymentDetailComponent implements OnInit {

  title = 'Edit Payment';
  form: FormGroup;
  trainings: TrainingListModel[];
  mentors: UserModel[];
  constructor(
    private paymentervice: PaymentService,
    private trainingService: TrainingService,
    private mentorService: MentorService,
    private dialogRef: MatDialogRef<PaymentDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PaymentModel) { }

  ngOnInit() {
    this.loadTrainings();
    this.loadMentors();
    this.buildForm();
  }
  
  buildForm() {
      
    this.form = new FormGroup({
      id: new FormControl(this.data.id),
      mentorId: new FormControl(this.data.mentorId, Validators.required),
      trainingId: new FormControl(this.data.trainingId, Validators.required),
      txnType: new FormControl(this.data.txnType, Validators.required),
      amount: new FormControl(this.data.amount, Validators.required),
      paymentDate: new FormControl(this.data.paymentDate, Validators.required),
      remarks: new FormControl(this.data.remarks, Validators.required)
    });
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close(null);
  }

 loadTrainings() {
    this.trainingService.getTrainingsForList().subscribe(data => {
      this.trainings = data;
      console.log(this.trainings);
    });
  }

  loadMentors() {
    this.mentorService.getMentors().subscribe(data => {
      this.mentors = data;
      console.log(this.mentors);
    });
  }
}

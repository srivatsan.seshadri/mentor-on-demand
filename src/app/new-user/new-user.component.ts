import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Logger } from '../core/logger/logger.service';

import { UserModel } from '../models/user-model';
import { RoleModel } from '../models/role-model';
import { UserService } from '../services/user.service';
import { UserDataComponent } from '../shared/forms/user-data/user-data.component';
import { TechnologyService } from '../services/technology.service';
import { TechnologyModel } from '../models/technology-model';
import { MentorSkillsModel } from '../models/mentor-skills-model';

@Component({
    selector: 'app-new-user',
    templateUrl: './new-user.component.html',
    styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {

    title = 'Create User';
    userForm: FormGroup;
    roles: RoleModel[];
    technologies: TechnologyModel[];
    mentorSkillColumns: String[];
    mentorSkills: MentorSkillsModel[];

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private techService: TechnologyService,
        private logger: Logger
    ) { }

    ngOnInit() {
        this.mentorSkills = [];
        this.buildUserForm();
        this.loadRoles();
        this.loadTechnologies();
        this.setMentorSkillsColumns();
        this.userForm.get('linkedInUrl').disable()
        this.userForm.get('yearsOfExperience').disable()
        this.userForm.get('mentorSkills').disable()
    }
    buildUserForm(): void {
        this.userForm = this.formBuilder.group({
            'userName': ['', Validators.required],
            'password': ['', Validators.required],
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'contactNumber': ['', Validators.required],
            'roleId': ['', Validators.required],
            'mentorSkills': [''],
            'linkedInUrl': [''],
            'yearsOfExperience': ['', Validators.required]
        });
    }

    save() {
        if (this.invalidForms())
            return;

        const newUser = this.getUser();
        this.logger.log(`New User: ${newUser}`);
        newUser.mentorSkills=this.mentorSkills;
        this.userService.addUser(newUser).subscribe(result => {
            if (result) {
                this.router.navigate(['/users']);
            }
        });
    }

    invalidForms(): boolean {
        return (this.userForm.invalid);
    }

    getUser(): UserModel {
        return this.userForm.value;
    }

    loadRoles() {
        this.userService.getRoles().subscribe(data => {
            this.roles = data;
            console.log(this.roles);
        });
    }
    loadTechnologies() {
        this.techService.getTechnologies().subscribe(data => {
            this.technologies = data;
            console.log(this.technologies);
        });
    }

    setMentorSkillsColumns() {
        this.mentorSkillColumns = ['skillName', 'selfRating', 'yearsOfExperience', 'trainingsDelivered', 'facilitiesOffered', 'action'];
    }

    addMentorData($event) {
        this.mentorSkills = $event;
    }
}
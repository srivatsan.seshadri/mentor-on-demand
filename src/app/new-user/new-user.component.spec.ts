import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {} from 'jasmine';
import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { Logger } from '../core/logger/logger.service';;

import { NewUserComponent } from './new-user.component';

import { UserModel } from '../models/user-model';
import { RoleModel } from '../models/role-model';
import { UserService } from '../services/user.service';
import { UserDataComponent } from '../shared/forms/user-data/user-data.component';
import { TechnologyService } from '../services/technology.service';
import { TechnologyModel } from '../models/technology-model';
import { MentorSkillsModel } from '../models/mentor-skills-model';

describe('NewUserComponent', () => {
  let component: NewUserComponent;
  let fixture: ComponentFixture<NewUserComponent>;

  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewUserComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,RouterTestingModule,HttpClientTestingModule,MatSnackBarModule],
      providers:[UserService,TechnologyService,{ provide: AppConfigService, useValue: mockedConfigService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

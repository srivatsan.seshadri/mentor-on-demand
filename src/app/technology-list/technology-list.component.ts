import { Component, HostListener, OnInit, ViewChild,ChangeDetectorRef  } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { MatTableDataSource } from '@angular/material';
import { ConfirmedDialogComponent } from '../shared/dialogs/confirmed-dialog/confirmed-dialog.component';
import { Logger } from '../core/logger/logger.service';
import { TechnologyModel } from '../models/technology-model';
import { TechDetailComponent } from '../tech-detail/tech-detail.component';
import { TechnologyService } from '../services/technology.service'

@Component({
  selector: 'app-technology-list',
  templateUrl: './technology-list.component.html',
  styleUrls: ['./technology-list.component.scss']
})
export class TechnologyListComponent implements OnInit {

  displayedColumns: string[];
    dataSource = new MatTableDataSource();
    screenHeight: any;
    screenWidth: any;

    @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
    @ViewChild(MatSort,{static: false}) sort: MatSort;

    @HostListener('window:resize', ['$event'])
        onResize(event?) {
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
        this.logger.log(`Resize() height: ${this.screenHeight}; width: ${this.screenWidth}`);
        this.setDisplayedColumns();
    }

  constructor(private techService: TechnologyService, 
        private logger: Logger, 
        private dialog: MatDialog,
        private cdr: ChangeDetectorRef) { 

          this.screenHeight = window.screen.height;
          this.screenWidth = window.screen.width;
          this.logger.log(`Init() height: ${this.screenHeight}; width: ${this.screenWidth}`);
          this.setDisplayedColumns();
        }

  ngOnInit() {
        this.loadTechnologies();
        this.cdr.detectChanges();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
  }
  
  applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

  loadTechnologies() {
        this.techService.getTechnologies().subscribe(data => {
            this.dataSource.data = data;
        });
    }
    
  editTechnology(id: number): void {
        
        this.techService.getTechnology(id).subscribe(data => {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.disableClose = true;
            dialogConfig.autoFocus = true;
            dialogConfig.height = '400px';
            dialogConfig.width = '500px';
            dialogConfig.data = data;
            
            const dialogRef = this.dialog.open(TechDetailComponent, dialogConfig);
            dialogRef.afterClosed().subscribe(result => {
                if (!result) {
                    return;
                }
                this.techService.updateTechnology(result)
                    .subscribe(_ => this.loadTechnologies());
            });
        });
    }

  deleteTechnology(tech: TechnologyModel): void {
        // Create configuration for the dialog
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.height = '200px';
        dialogConfig.width = '400px';
        dialogConfig.data = { 
            title: `Delete  ${tech.name}`,
            message: 'Are you sure to delete this Tehchnology?'
        };

        const dialogRef = this.dialog.open(ConfirmedDialogComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.dataSource.data = this.dataSource.data.filter(e => e != tech);
                this.techService.deleteTechnology(tech).subscribe();
            }
        });
    }

    setDisplayedColumns() {
        if (this.screenWidth < 420) {
            this.displayedColumns = ['name', 'termsOfConditions', 'duration'];
        }
        else if (this.screenWidth >= 420 && this.screenWidth <= 800) {
            this.displayedColumns = ['name', 'termsOfConditions', 'duration','prerequisites'];
        }
        else {
            this.displayedColumns = ['name', 'termsOfConditions', 'duration','prerequisites', 'action'];
        }
    }

}

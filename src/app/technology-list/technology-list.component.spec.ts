import {} from 'jasmine';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';

import { TechnologyListComponent } from './technology-list.component';
import { Component, HostListener, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatDialogModule} from '@angular/material';
import { MatTableModule ,MatTableDataSource } from '@angular/material';
import { ConfirmedDialogComponent } from '../shared/dialogs/confirmed-dialog/confirmed-dialog.component';
import { Logger } from '../core/logger/logger.service';
import { TechnologyModel } from '../models/technology-model';
import { TechDetailComponent } from '../tech-detail/tech-detail.component';
import { TechnologyService } from '../services/technology.service'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('TechnologyListComponent', () => {
  let component: TechnologyListComponent;
  let fixture: ComponentFixture<TechnologyListComponent>;
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnologyListComponent,ConfirmedDialogComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,
               RouterTestingModule,
               HttpClientTestingModule,
               MatSnackBarModule,
               MatPaginatorModule,
               MatSortModule,
               MatTableModule,
               MatDialogModule,
               BrowserAnimationsModule],
      providers:[TechnologyService,{ provide: AppConfigService, useValue: mockedConfigService}]
    })
    .compileComponents();
    configService = TestBed.get(AppConfigService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnologyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

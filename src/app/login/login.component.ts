import { Component, OnInit, Injectable, Inject } from '@angular/core';
import { LoginModel } from '../models/login-model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map,retry, catchError } from 'rxjs/operators';
import { AppConfigService } from '../config/app-config.module';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    private user: LoginModel = new LoginModel();
    private apiBaseUrl: string;

    constructor(private router: Router,private http: HttpClient, private appConfigService: AppConfigService

    ) {

    }

    ngOnInit() {
        this.apiBaseUrl = this.appConfigService.apiUrl.authApiEndpoint;
    }

    login(login: LoginModel) {

        console.log(login);
        return this.http.post(
             this.apiBaseUrl + "ValidateUser", 
           login).subscribe(data=>{
                console.log(data);
                localStorage.setItem('currentUser', JSON.stringify(data));
                this.router.navigate(['/home']);
           });
    }

    logout() {
        localStorage.removeItem('currentUser');
    }

    errorHandler(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        throwError(errorMessage);
    }
}

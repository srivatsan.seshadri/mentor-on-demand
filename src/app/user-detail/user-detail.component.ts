import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { UserModel } from '../models/user-model';
import { RoleModel } from '../models/role-model';
import { UserDataComponent } from '../shared/forms/user-data/user-data.component';
import { UserService } from '../services/user.service';
import { TechnologyService } from '../services/technology.service';
import { TechnologyModel } from '../models/technology-model';
import { MentorSkillsModel } from '../models/mentor-skills-model';
@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
    title = 'Edit User';
    form: FormGroup;
    roles: RoleModel[];
    technologies: TechnologyModel[];
    mentorSkillColumns: String[];
    mentorSkills: MentorSkillsModel[];
    constructor(
        private userService: UserService,
        private techService: TechnologyService,
        private dialogRef: MatDialogRef<UserDetailComponent>,
        @Inject(MAT_DIALOG_DATA) public data: UserModel) { }

    ngOnInit() {
        this.mentorSkills = [];
        this.loadRoles();
        this.loadTechnologies();
        this.loadMentorSkills(this.data.id);
        this.setMentorSkillsColumns();
        this.buildForm();
    }

    buildForm() {

        this.form = new FormGroup({
            id: new FormControl(this.data.id),
            userName: new FormControl(this.data.userName, Validators.required),
            password: new FormControl(this.data.password, Validators.required),
            firstName: new FormControl(this.data.firstName, Validators.required),
            lastName: new FormControl(this.data.lastName, Validators.required),
            contactNumber: new FormControl(this.data.contactNumber, Validators.required),
            roleId: new FormControl(this.data.roleId, Validators.required),
            mentorSkills: new FormControl(this.mentorSkills),
            linkedInUrl: new FormControl(this.data.linkedInUrl),
            yearsOfExperience: new FormControl(this.data.yearsOfExperience),
            regCode:new FormControl(this.data.regCode)
        });
    }

    save() {
        if (this.form.invalid) {
            return;
        }
        this.form.value.mentorSkills=this.mentorSkills;
        this.dialogRef.close(this.form.value);
    }

    close() {
        this.dialogRef.close(null);
    }

    loadRoles() {
        this.userService.getRoles().subscribe(data => {
            this.roles = data;
            console.log(this.roles);
        });
    }

    loadMentorSkills(id) {
        this.userService.getMentorSkills(id).subscribe(data => {
            this.mentorSkills = data;
            console.log(this.mentorSkills);
        });
    }

    loadTechnologies() {
        this.techService.getTechnologies().subscribe(data => {
            this.technologies = data;
            console.log(this.technologies);
        });
    }

    setMentorSkillsColumns() {
        this.mentorSkillColumns = ['skillName', 'selfRating', 'yearsOfExperience', 'trainingsDelivered', 'facilitiesOffered', 'action'];
    }

    addMentorData($event) {
        this.mentorSkills = $event;
        this.form.markAsDirty()
    }
}

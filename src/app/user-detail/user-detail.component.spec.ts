import {} from 'jasmine';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormControl, Validators,FormBuilder, FormGroup,ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { Logger } from '../core/logger/logger.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { UserDetailComponent } from './user-detail.component';
import { UserModel } from '../models/user-model';
import { RoleModel } from '../models/role-model';
import { UserDataComponent } from '../shared/forms/user-data/user-data.component';
import { UserService } from '../services/user.service';
import { TechnologyService } from '../services/technology.service';
import { TechnologyModel } from '../models/technology-model';
import { MentorSkillsModel } from '../models/mentor-skills-model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatDialogModule, MatTableModule } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule } from '@angular/material/radio';
import {MatInputModule} from '@angular/material';

describe('UserDetailComponent', () => {
  let component: UserDetailComponent;
  let fixture: ComponentFixture<UserDetailComponent>;

  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDetailComponent,UserDataComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,
               RouterTestingModule,
               HttpClientTestingModule,
               MatSnackBarModule,
               MatDialogModule,
               MatTableModule,
               MatSelectModule,
               MatRadioModule,
               MatInputModule,
               BrowserAnimationsModule],
      providers:[UserService,TechnologyService,{ provide: AppConfigService, useValue: mockedConfigService},{ provide: MAT_DIALOG_DATA, useValue: {} },{ provide: MatDialogRef, useValue: {} }]
    })
    .compileComponents();
    configService = TestBed.get(AppConfigService);
  }));

  beforeEach(() => {
    var mentorSkills:MentorSkillsModel[];
    fixture = TestBed.createComponent(UserDetailComponent);
    component = fixture.componentInstance;
    component.form =new FormGroup({
            id: new FormControl(''),
            userName: new FormControl(''),
            password: new FormControl(''),
            firstName: new FormControl(''),
            lastName: new FormControl(''),
            contactNumber: new FormControl(''),
            roleId: new FormControl(''),
            mentorSkills: new FormControl(mentorSkills),
            linkedInUrl: new FormControl(''),
            yearsOfExperience: new FormControl(''),
            regCode:new FormControl('')
        });
    fixture.detectChanges();
  });

  it('should create', () => {
    var mentorSkills:MentorSkillsModel[];
    var roles: RoleModel[];
    var technologies: TechnologyModel[];
    var mentorSkillColumns: String[];
    var mentorSkills: MentorSkillsModel[];
    component.form =new FormGroup({
            id: new FormControl(''),
            userName: new FormControl(''),
            password: new FormControl(''),
            firstName: new FormControl(''),
            lastName: new FormControl(''),
            contactNumber: new FormControl(''),
            roleId: new FormControl(''),
            mentorSkills: new FormControl(mentorSkills),
            linkedInUrl: new FormControl(''),
            yearsOfExperience: new FormControl(''),
            regCode:new FormControl('')
        });
    component.roles=roles;
    component.technologies=technologies;
    component.mentorSkillColumns=mentorSkillColumns;
    component.mentorSkills=mentorSkills;
    expect(component).toBeTruthy();
  });
});

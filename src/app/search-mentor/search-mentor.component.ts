import { Moment } from 'moment';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { NavbarComponent } from '../navbar/navbar.component'
import { UserModel } from '../models/user-model';
import { SearchResultModel } from '../models/search-result-model';
import { TechnologyModel } from '../models/technology-model';
import { ProposalModel } from '../models/proposal-model';
import { ProposalService } from '../services/proposal.service';
import { SearchService } from '../services/search.service';
import { UserService } from '../services/user.service'
import { TechnologyService } from '../services/technology.service';
import { ToastService } from '../core/toast/toast.service';

@Component({
  selector: 'app-search-mentor',
  templateUrl: './search-mentor.component.html',
  styleUrls: ['./search-mentor.component.scss']
})

export class SearchMentorComponent implements OnInit {

  form: FormGroup;
  filteredUsers: UserModel[];
  searchResults: SearchResultModel[];
  proposals: ProposalModel[];
  technologies:TechnologyModel[];

  constructor(
    private searchService: SearchService,
    private userService: UserService,
    private proposalService: ProposalService,
    private techService: TechnologyService,
    private toastService: ToastService) { }

  ngOnInit() {
    this.loadTechnologies();
    this.buildForm();
  }

  buildForm() {

    this.form = new FormGroup({
      technology: new FormControl('0'),
      fromDate: new FormControl(''),
      toDate: new FormControl('')
    });

  }
  loadTechnologies() {
    this.techService.getTechnologies().subscribe(data => {
      this.technologies = data;
      console.log(this.technologies);
    });
  }
  search(form: any) {
    
    let frmDate:Date = new Date(form.fromDate.year + "/" + form.fromDate.month + "/" + form.fromDate.day);
    let toDate:Date = new Date(form.toDate.year + "/" + form.toDate.month + "/" + form.toDate.day);
     this.searchService.getSearchResult(this.form.value.technology,frmDate,toDate).subscribe(data => {
      this.searchResults = data;
    });
  }

  propose(searchResult: SearchResultModel) {
    // this.userService.getUser(searchResult.id).subscribe(data => {
    //   let proposal = new ProposalModel();
    //   proposal.mentorId = searchResult.id;
    //   proposal.firstName = data.firstName;
    //   proposal.lastName = data.lastName;
    //   proposal.email = "";
    //   proposal.proposalDate=null;
    //   proposal.mobile = data.contactNumber;
    //   proposal.isApproved = false;

    //   this.proposalService.addProposal(proposal);
    // });
    this.notify("The Proposal Details have been sent to the Mentor. You would receive further communications from the Mentor.","")
  }
  protected notify(message: string, method: string) {
        this.toastService.openSnackBar(message, method);
    }
}

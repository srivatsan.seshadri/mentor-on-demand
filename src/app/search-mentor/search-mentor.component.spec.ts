import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {} from 'jasmine';
import { SearchMentorComponent } from './search-mentor.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NavbarComponent } from '../navbar/navbar.component'
import { UserModel } from '../models/user-model';
import { SearchResultModel } from '../models/search-result-model';
import { TechnologyModel } from '../models/technology-model';
import { ProposalModel } from '../models/proposal-model';
import { ProposalService } from '../services/proposal.service';
import { SearchService } from '../services/search.service';
import { UserService } from '../services/user.service'
import { TechnologyService } from '../services/technology.service';
import { ToastService } from '../core/toast/toast.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
describe('SearchMentorComponent', () => {
  let component: SearchMentorComponent;
  let fixture: ComponentFixture<SearchMentorComponent>;
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchMentorComponent,NavbarComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,RouterTestingModule,HttpClientTestingModule,BrowserAnimationsModule,NgbModule,MatSnackBarModule],
      providers:[ProposalService,SearchService,UserService,ToastService,TechnologyService,{ provide: AppConfigService, useValue: mockedConfigService}]
    })
    .compileComponents();
    configService = TestBed.get(AppConfigService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMentorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, HostListener, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { MatTableDataSource } from '@angular/material';
import { ConfirmedDialogComponent } from '../shared/dialogs/confirmed-dialog/confirmed-dialog.component';
import { Logger } from '../core/logger/logger.service';
import { MentorModel } from '../models/mentor-model';
import { PaymentModel } from '../models/payment-model';
import { PaymentListModel } from '../models/payment-list-model';
import { TrainingModel } from '../models/training-model';
import { PaymentDetailComponent } from '../payment-detail/payment-detail.component';
import { PaymentService } from '../services/payment.service'
import { TrainingService } from '../services/training.service'
import { MentorService } from '../services/mentor.service'

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.scss']
})
export class PaymentListComponent implements OnInit {

  displayedColumns: string[];
  dataSource = new MatTableDataSource();
  screenHeight: any;
  screenWidth: any;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    this.logger.log(`Resize() height: ${this.screenHeight}; width: ${this.screenWidth}`);
    this.setDisplayedColumns();
  }

  trainings: TrainingModel[];
  mentors: MentorModel[];
  constructor(private paymentService: PaymentService,
    private mentorService: MentorService,
    private trainingService: TrainingService,
    private logger: Logger,
    private dialog: MatDialog,
    private cdr: ChangeDetectorRef) {

    this.screenHeight = window.screen.height;
    this.screenWidth = window.screen.width;
    this.logger.log(`Init() height: ${this.screenHeight}; width: ${this.screenWidth}`);
    this.setDisplayedColumns();
  }

  ngOnInit() {
    // this.loadMentors();
    // this.loadTrainings();
    this.loadPayments();
    this.cdr.detectChanges();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  loadMentors() {
    this.mentorService.getMentors().subscribe(data => {
      this.mentors = data;
    });
  }
  loadTrainings() {
    this.trainingService.getTrainings().subscribe(data => {
      this.trainings = data;
    });
  }

  loadPayments() {
    this.paymentService.getPaymentsList().subscribe(data => {
      this.dataSource.data = data;
    });
  }

  editPayment(id: number): void {

    this.paymentService.getPayment(id).subscribe(data => {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.height = '400px';
      dialogConfig.width = '500px';
      dialogConfig.data = data;

      const dialogRef = this.dialog.open(PaymentDetailComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (!result) {
          return;
        }
        this.paymentService.updatePayment(result)
          .subscribe(_ => this.loadPayments());
      });
    });
  }

  deletePayment(payment: PaymentModel): void {
    // Create configuration for the dialog
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.height = '200px';
    dialogConfig.width = '400px';
    dialogConfig.data = {
      title: `Delete Payment for Payment ${payment.training}`,
      message: 'Are you sure to delete this Payment?'
    };

    const dialogRef = this.dialog.open(ConfirmedDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dataSource.data = this.dataSource.data.filter(e => e != payment);
        this.paymentService.deletePayment(payment).subscribe();
      }
    });
  }

  setDisplayedColumns() {

    if (this.screenWidth < 420) {
      this.displayedColumns = ['mentor', 'training', 'txtType','action'];
    }
    else if (this.screenWidth >= 420 && this.screenWidth <= 800) {
      this.displayedColumns = ['mentor', 'training', 'txtType', 'amount','action'];
    }
    else {
      this.displayedColumns = ['mentor', 'training', 'txtType', 'amount', 'paymentDate','action'];
    }
  }

  getFullName(mentor: number): string {
    var user = '';
    this.mentorService.getUser(mentor).subscribe(data => {
      user = `${data.firstName} ${data.lastName}`;
    });
    return user;
  }

  getTrainingName(id: number): string {
    var training: TrainingModel;
    this.trainingService.getTraining(id).subscribe(data => {
      training = data;
    });

    return '';
  }

}

import {} from 'jasmine';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';
import { PaymentListComponent } from './payment-list.component';
import { Component, HostListener, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatDialogModule} from '@angular/material';
import { MatTableModule ,MatTableDataSource } from '@angular/material';
import { ConfirmedDialogComponent } from '../shared/dialogs/confirmed-dialog/confirmed-dialog.component';

import { Logger } from '../core/logger/logger.service';
import { MentorModel } from '../models/mentor-model';
import { PaymentModel } from '../models/payment-model';
import { PaymentListModel } from '../models/payment-list-model';
import { TrainingModel } from '../models/training-model';
import { PaymentDetailComponent } from '../payment-detail/payment-detail.component';
import { PaymentService } from '../services/payment.service'
import { TrainingService } from '../services/training.service'
import { MentorService } from '../services/mentor.service'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PaymentListComponent', () => {
  let component: PaymentListComponent;
  let fixture: ComponentFixture<PaymentListComponent>;
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentListComponent,ConfirmedDialogComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,RouterTestingModule,HttpClientTestingModule,MatSnackBarModule,MatPaginatorModule,MatSortModule,MatTableModule,MatDialogModule,BrowserAnimationsModule],
      providers:[TrainingService,PaymentService,MentorService,{ provide: AppConfigService, useValue: mockedConfigService}]
    })
    .compileComponents();
    configService = TestBed.get(AppConfigService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

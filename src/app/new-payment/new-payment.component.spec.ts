import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { NewPaymentComponent } from './new-payment.component';
import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { Router } from '@angular/router';
import { Logger } from '../core/logger/logger.service';
import { PaymentModel } from '../models/payment-model';
import { PaymentService } from '../services/payment.service';
import { PaymentDataComponent } from '../shared/forms/payment-data/payment-data.component';
import { MentorService } from '../services/mentor.service';
import { TrainingService } from '../services/training.service';
import { TrainingModel } from '../models/training-model';
import { TrainingListModel } from '../models/training-list-model';
import { UserModel } from '../models/user-model';
import {} from 'jasmine';
describe('NewPaymentComponent', () => {
  let component: NewPaymentComponent;
  let fixture: ComponentFixture<NewPaymentComponent>;
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPaymentComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,RouterTestingModule,HttpClientTestingModule,MatSnackBarModule],
      providers:[PaymentService,TrainingService,MentorService,{ provide: AppConfigService, useValue: mockedConfigService}]
    })
    .compileComponents();
    configService = TestBed.get(AppConfigService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

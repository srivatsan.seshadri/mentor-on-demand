import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Logger } from '../core/logger/logger.service';
import { PaymentModel } from '../models/payment-model';
import { PaymentService } from '../services/payment.service';
import { PaymentDataComponent } from '../shared/forms/payment-data/payment-data.component';
import { MentorService } from '../services/mentor.service';
import { TrainingService } from '../services/training.service';
import { TrainingModel } from '../models/training-model';
import { TrainingListModel } from '../models/training-list-model';
import { UserModel } from '../models/user-model';

@Component({
  selector: 'app-new-payment',
  templateUrl: './new-payment.component.html',
  styleUrls: ['./new-payment.component.scss']
})
export class NewPaymentComponent implements OnInit {

  title = 'Create Payment';
  paymentForm: FormGroup;
  trainings: TrainingListModel[];
  mentors: UserModel[];

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private paymentService: PaymentService,
    private trainingService: TrainingService,
    private mentorService: MentorService,
    private logger: Logger) { }

  ngOnInit() {
    this.loadTrainings();
    this.loadMentors();
    this.buildPaymentForm();
  }

  buildPaymentForm(): void {

    this.paymentForm = this.formBuilder.group({
      'mentorId': ['', Validators.required],
      'trainingId': ['', Validators.required],
      'txnType': ['', Validators.required],
      'amount': ['', Validators.required],
      'paymentDate': ['', Validators.required],
      'remarks': ['', Validators.required]
    });
  }

  save() {
    if (this.invalidForms())
      return;

    const newPayment = this.getPayment();
    this.logger.log(`New Technology: ${newPayment}`);

    this.paymentService.addPayment(newPayment).subscribe(result => {
      if (result) {
        this.router.navigate(['/payments']);
      }
    });
  }

  invalidForms(): boolean {
    return (this.paymentForm.invalid);
  }

  getPayment(): PaymentModel {
    return this.paymentForm.value;
  }

  loadTrainings() {
    this.trainingService.getTrainingsForList().subscribe(data => {
      this.trainings = data;
      console.log(this.trainings);
    });
  }

  loadMentors() {
    this.mentorService.getMentors().subscribe(data => {
      this.mentors = data;
      console.log(this.mentors);
    });
  }
}

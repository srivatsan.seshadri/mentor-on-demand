import {} from 'jasmine';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';

import { UserListComponent } from './user-list.component';

import {NavbarComponent} from '../navbar/navbar.component'
import { Component, HostListener, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatDialogModule} from '@angular/material';
import { MatTableModule ,MatTableDataSource } from '@angular/material';
import { ConfirmedDialogComponent } from '../shared/dialogs/confirmed-dialog/confirmed-dialog.component';
import { Logger } from '../core/logger/logger.service';
import { UserModel } from '../models/user-model';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { UserService } from '../services/user.service'
import { MentorSkillsModel } from '../models/mentor-skills-model';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;

  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserListComponent,UserDetailComponent,NavbarComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,
               RouterTestingModule,
               HttpClientTestingModule,
               MatSnackBarModule,
               MatPaginatorModule,
               MatSortModule,
               MatTableModule,
               MatDialogModule,
               BrowserAnimationsModule],
      providers:[UserService,{ provide: AppConfigService, useValue: mockedConfigService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

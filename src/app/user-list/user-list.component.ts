import { Component, HostListener, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { MatTableDataSource } from '@angular/material';
import { ConfirmedDialogComponent } from '../shared/dialogs/confirmed-dialog/confirmed-dialog.component';
import { Logger } from '../core/logger/logger.service';
import { UserModel } from '../models/user-model';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { UserService } from '../services/user.service'
import { MentorSkillsModel } from '../models/mentor-skills-model';
@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
    mentorSkills: MentorSkillsModel[];
    displayedColumns: string[];
    dataSource = new MatTableDataSource();
    screenHeight: any;
    screenWidth: any;

    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false }) sort: MatSort;

    @HostListener('window:resize', ['$event'])
    onResize(event?) {
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
        this.logger.log(`Resize() height: ${this.screenHeight}; width: ${this.screenWidth}`);
        this.setDisplayedColumns();
    }

    constructor(private userService: UserService,
        private logger: Logger,
        private dialog: MatDialog,
        private cdr: ChangeDetectorRef) {

        this.screenHeight = window.screen.height;
        this.screenWidth = window.screen.width;
        this.logger.log(`Init() height: ${this.screenHeight}; width: ${this.screenWidth}`);
        this.setDisplayedColumns();
    }

    ngOnInit() {
        this.loadUsers();
        this.cdr.detectChanges();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    loadUsers() {
        this.userService.getUsers().subscribe(data => {
            this.dataSource.data = data;
        });
    }

    editUser(id: number): void {

        this.userService.getUser(id).subscribe(data => {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.disableClose = true;
            dialogConfig.autoFocus = true;
            dialogConfig.height = '400px';
            dialogConfig.width = '500px';
            dialogConfig.data = data;

            const dialogRef = this.dialog.open(UserDetailComponent, dialogConfig);
            dialogRef.afterClosed().subscribe(result => {
                if (!result) {
                    return;
                }
                this.userService.updateUser(result)
                    .subscribe(_ => this.loadUsers());
            });
        });
    }

    deleteUser(user: UserModel): void {
        // Create configuration for the dialog
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.height = '200px';
        dialogConfig.width = '400px';
        dialogConfig.data = {
            title: `Delete  ${user.firstName} ${user.lastName}`,
            message: 'Are you sure?'
        };

        const dialogRef = this.dialog.open(ConfirmedDialogComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.dataSource.data = this.dataSource.data.filter(e => e != user);
                this.userService.deleteUser(user).subscribe();
            }
        });
    }

    getFullName(user: UserModel): string {
        return `${user.firstName} ${user.lastName}`;
    }

    setDisplayedColumns() {
        if (this.screenWidth < 420) {
            this.displayedColumns = ['userName', 'fullName', 'contact'];
        }
        else if (this.screenWidth >= 420 && this.screenWidth <= 800) {
            this.displayedColumns = ['userName', 'fullName', 'contact', 'regCode', 'action'];
        }
        else {
            this.displayedColumns = ['userName', 'fullName', 'contact', 'regCode', 'role', 'action'];
        }
    }
    addMentorData($event) {
        this.mentorSkills = $event;
    }
}

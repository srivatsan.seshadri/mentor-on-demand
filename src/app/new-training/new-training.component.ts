import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Logger } from '../core/logger/logger.service';
import { UserModel } from '../models/user-model';
import { TechnologyModel } from '../models/technology-model';
import { TrainingModel } from '../models/training-model';
import { TrainingService } from '../services/training.service';
import { TrainingDataComponent } from '../shared/forms/training-data/training-data.component';
import { MentorService } from '../services/mentor.service';
import { TechnologyService } from '../services/technology.service';

@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.scss']
})

export class NewTrainingComponent implements OnInit {

  title = 'Create Training';
  trainingForm: FormGroup;
  technologies: TechnologyModel[];
  mentors: UserModel[];
  
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private trainingService: TrainingService,
    private mentorService: MentorService,
    private techService: TechnologyService,
    private logger: Logger) { }

  ngOnInit() {
    this.buildTrainingForm();
    this.loadTechnologies();
    this.loadMentors();
  }

  buildTrainingForm(): void {

    this.trainingForm = this.formBuilder.group({
      'mentorId': [''],
      'skillId': [''],
      'status': [''],
      'progress': [''],
      'rating': [''],
      'startDate': [''],
      'endDate': [''],
      'fees':['']
    });
  }

  save() {
    if (this.invalidForms())
      return;

    const newTraining = this.getTraining();

    this.logger.log(`New Training: ${newTraining}`);

    this.trainingService.addTraining(newTraining).subscribe(result => {
      if (result) {
        this.router.navigate(['/trainings']);
      }
    });
  }

  invalidForms(): boolean {
    return (this.trainingForm.invalid);
  }

  getTraining(): TrainingModel {
    return this.trainingForm.value;
  }

  loadTechnologies() {
    this.techService.getTechnologies().subscribe(data => {
      this.technologies = data;
      console.log(this.technologies);
    });
  }

  loadMentors() {
    this.mentorService.getMentors().subscribe(data => {
      this.mentors = data;
      console.log(this.mentors);
    });
  }
}

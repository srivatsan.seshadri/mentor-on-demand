import { Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MentorSkillsModel } from '../../../models/mentor-skills-model';
import { TechnologyModel } from '../../../models/technology-model';

@Component({
  selector: 'app-form-dialog',
  templateUrl: './form-dialog.component.html',
  styleUrls: ['./form-dialog.component.scss']
})
export class FormDialogComponent {

  action: string;
  local_data: any;
  technologies:TechnologyModel[];
  constructor(
    public dialogRef: MatDialogRef<FormDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data);
    this.local_data = { ...data.mentorSkill };
    this.action = this.local_data.action;
    this.technologies=data.technologies;
    this.local_data.skill=this.technologies.find(s=>s.id==this.local_data.skillId);
  }

  doAction() {
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }

}

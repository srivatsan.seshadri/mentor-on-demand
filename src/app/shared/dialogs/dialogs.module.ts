import { NgModule } from '@angular/core';

import { MatDialogModule } from '@angular/material/dialog';

import { ConfirmedDialogComponent } from './confirmed-dialog/confirmed-dialog.component';
import { FormDialogComponent } from './form-dialog/form-dialog.component';

@NgModule({
    imports: [
        MatDialogModule
    ],
    declarations: [
        ConfirmedDialogComponent,
        FormDialogComponent
    ],
    exports:[ 
        ConfirmedDialogComponent,
        FormDialogComponent
    ],
    entryComponents: [
        ConfirmedDialogComponent,
        FormDialogComponent
    ]
})

export class DialogsModule { }

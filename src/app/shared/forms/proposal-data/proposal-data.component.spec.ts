import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalDataComponent } from './proposal-data.component';
import {} from 'jasmine';
describe('ProposalDataComponent', () => {
  let component: ProposalDataComponent;
  let fixture: ComponentFixture<ProposalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProposalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

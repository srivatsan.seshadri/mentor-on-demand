import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TrainingDataComponent } from './training-data.component';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';
import { UserModel } from '../../../models/user-model';
import { TechnologyModel } from '../../../models/technology-model';
import { FormControl, Validators, FormGroup, ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material';
import {} from 'jasmine';
describe('TrainingDataComponent', () => {
  let component: TrainingDataComponent;
  let fixture: ComponentFixture<TrainingDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TrainingDataComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ReactiveFormsModule,
        NgxMatDatetimePickerModule,
        NgxMatTimepickerModule,
        NgxMatNativeDateModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatRadioModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingDataComponent);
    component = fixture.componentInstance;
    component.form = new FormGroup({
      id: new FormControl(''),
      mentorId: new FormControl(''),
      skillId: new FormControl(''),
      status: new FormControl(''),
      progress: new FormControl(''),
      rating: new FormControl(''),
      startDate: new FormControl(''),
      endDate: new FormControl(''),
      fees: new FormControl(''),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    component.form = new FormGroup({
      id: new FormControl(''),
      mentorId: new FormControl(''),
      skillId: new FormControl(''),
      status: new FormControl(''),
      progress: new FormControl(''),
      rating: new FormControl(''),
      startDate: new FormControl(''),
      endDate: new FormControl(''),
      fees: new FormControl(''),
    });
    
    var technologies: TechnologyModel[];
    var users: UserModel[];
    
    component.mentors = users;
    component.technologies = technologies;

    expect(component).toBeTruthy();
  });
});

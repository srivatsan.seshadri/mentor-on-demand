import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UserModel } from '../../../models/user-model';
import { TechnologyModel } from '../../../models/technology-model';
 
@Component({
  selector: 'app-training-data',
  templateUrl: './training-data.component.html',
  styleUrls: ['./training-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
  
export class TrainingDataComponent {
  @Input() form: FormGroup;
  @Input() technologies: TechnologyModel[];
  @Input() mentors: UserModel[];
  constructor() { }

}

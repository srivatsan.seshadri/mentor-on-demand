import { ChangeDetectionStrategy, Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatTable } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { RoleModel } from '../../../models/role-model';
import { TechnologyModel } from '../../../models/technology-model';
import { MentorSkillsModel } from '../../../models/mentor-skills-model';
import { FormDialogComponent } from '../../dialogs/form-dialog/form-dialog.component';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserDataComponent {

  // Receive FormGroup instance from the parent 'NewContactComponent'
  @Input() form: FormGroup;
  @Input() roles: RoleModel[];
  @Input() technologies: TechnologyModel[];
  @Input() mentorSkills: MentorSkillsModel[];
  @Input() mentorSkillColumns: string[];
  @Output() mentorSkillsOutput = new EventEmitter<MentorSkillsModel[]>();
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(public dialog: MatDialog) { }

  onChange(data) {
    this.form.get('linkedInUrl').setValue('');
    this.form.get('yearsOfExperience').setValue('');
    this.form.get('mentorSkills').setValue(null);

    if (data && data === 3) {
      this.form.get('linkedInUrl').enable();
      this.form.get('yearsOfExperience').enable();
      this.form.get('mentorSkills').enable()
    } else {
      this.form.get('linkedInUrl').disable()
      this.form.get('yearsOfExperience').disable()
      this.form.get('mentorSkills').disable()
    }
  }

  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(FormDialogComponent, {
      width: '650px',
      data: { mentorSkill: obj, technologies: this.technologies }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Add') {
        this.addRowData(result.data);
      } else if (result.event == 'Update') {
        this.updateRowData(result.data);
      } else if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
      this.mentorSkillsOutput.emit(this.mentorSkills);
    });

  }

  addRowData(row_obj) {
    var data = this.mentorSkills.filter((value, key) => {
      if (value.skillId == row_obj.skillId)
        return true;
    });
    if (data.length>0)
      this.updateRowData(row_obj);
    else
      this.mentorSkills.push({
        id: 0,
        mentorId: 0,
        skillId: row_obj.skill.id,
        skillName: row_obj.skill.name,
        skill: row_obj.skill,
        selfRating: row_obj.selfRating,
        yearsOfExperience: row_obj.yearsOfExperience,
        trainingsDelivered: row_obj.trainingsDelivered,
        facilitiesOffered: row_obj.facilitiesOffered
      });
    this.table.renderRows();

  }
  updateRowData(row_obj) {
    this.mentorSkills = this.mentorSkills.filter((value, key) => {
      if (value.skillId == row_obj.skillId) {
        value.id = row_obj.id;
        value.mentorId = row_obj.mentorId;
        value.skillId = row_obj.skill.id;
        value.skillName = row_obj.skill.name;
        value.skill = row_obj.skill;
        value.selfRating = row_obj.selfRating;
        value.yearsOfExperience = row_obj.yearsOfExperience;
        value.trainingsDelivered = row_obj.trainingsDelivered;
        value.facilitiesOffered = row_obj.facilitiesOffered;
      }
      return true;
    });
  }
  deleteRowData(row_obj) {
    this.mentorSkills = this.mentorSkills.filter((value, key) => {
      return value.skillId != row_obj.skillId;
    });
  }
}

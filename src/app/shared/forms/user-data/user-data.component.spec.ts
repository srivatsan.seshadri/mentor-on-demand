import {} from 'jasmine';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { UserDataComponent } from './user-data.component';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';
import { MatDialogModule, MatTableModule } from '@angular/material';

import { FormControl, Validators, FormGroup, ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material';
import { FormDialogComponent } from '../../dialogs/form-dialog/form-dialog.component';
import { RoleModel } from '../../../models/role-model';
import { TechnologyModel } from '../../../models/technology-model';
import { MentorSkillsModel } from '../../../models/mentor-skills-model';

describe('UserDataComponent', () => {
  let component: UserDataComponent;
  let fixture: ComponentFixture<UserDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDataComponent,FormDialogComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ReactiveFormsModule,
        NgxMatDatetimePickerModule,
        NgxMatTimepickerModule,
        NgxMatNativeDateModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatRadioModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatDialogModule,
        MatTableModule,
        FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    var mentorSkills:MentorSkillsModel[];
    fixture = TestBed.createComponent(UserDataComponent);
    component = fixture.componentInstance;
    component.form =new FormGroup({
            id: new FormControl(''),
            userName: new FormControl(''),
            password: new FormControl(''),
            firstName: new FormControl(''),
            lastName: new FormControl(''),
            contactNumber: new FormControl(''),
            roleId: new FormControl(''),
            mentorSkills: new FormControl(mentorSkills),
            linkedInUrl: new FormControl(''),
            yearsOfExperience: new FormControl(''),
            regCode:new FormControl('')
        });
    fixture.detectChanges();
  });

  it('should create', () => {
    var mentorSkills:MentorSkillsModel[];
    var roles: RoleModel[];
    var technologies: TechnologyModel[];
    var mentorSkillColumns: string[];
    component.form =new FormGroup({
            id: new FormControl(''),
            userName: new FormControl(''),
            password: new FormControl(''),
            firstName: new FormControl(''),
            lastName: new FormControl(''),
            contactNumber: new FormControl(''),
            roleId: new FormControl(''),
            mentorSkills: new FormControl(mentorSkills),
            linkedInUrl: new FormControl(''),
            yearsOfExperience: new FormControl(''),
            regCode:new FormControl('')
        });
    component.mentorSkillColumns=mentorSkillColumns;
    component.mentorSkills=mentorSkills;
    component.roles=roles;
    component.technologies=technologies;
    expect(component).toBeTruthy();
  });
});

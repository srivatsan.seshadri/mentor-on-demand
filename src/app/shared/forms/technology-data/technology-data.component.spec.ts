import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TechnologyDataComponent } from './technology-data.component';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';
import { UserModel } from '../../../models/user-model';
import { TrainingModel } from '../../../models/training-model';
import {FormControl, Validators, FormGroup, ReactiveFormsModule, FormsModule} from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule } from '@angular/material/radio';
import {MatInputModule} from '@angular/material';
import {} from 'jasmine';
describe('TechnologyDataComponent', () => {
  let component: TechnologyDataComponent;
  let fixture: ComponentFixture<TechnologyDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnologyDataComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ReactiveFormsModule,
                NgxMatDatetimePickerModule, 
                NgxMatTimepickerModule, 
                NgxMatNativeDateModule, 
                ReactiveFormsModule,
                MatSelectModule,
                MatRadioModule,
                MatFormFieldModule,
                MatInputModule,
                BrowserAnimationsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnologyDataComponent);
    component = fixture.componentInstance;
    component.form =  new FormGroup({
            id: new FormControl(''),
            name: new FormControl(''),
            termsOfConditions: new FormControl(''),
            duration: new FormControl(''),
            prerequisites: new FormControl('')
        });
    fixture.detectChanges();
  });

  it('should create', () => {
    component.form =  new FormGroup({
            id: new FormControl(''),
            name: new FormControl(''),
            termsOfConditions: new FormControl(''),
            duration: new FormControl(''),
            prerequisites: new FormControl('')
        });
    expect(component).toBeTruthy();
  });
});

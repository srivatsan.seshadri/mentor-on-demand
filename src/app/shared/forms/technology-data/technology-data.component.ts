import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-technology-data',
  templateUrl: './technology-data.component.html',
  styleUrls: ['./technology-data.component.scss'],
   changeDetection: ChangeDetectionStrategy.OnPush
})
export class TechnologyDataComponent{
 @Input() form: FormGroup;
  constructor() { }

}

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UserModel } from '../../../models/user-model';
import { TrainingModel } from '../../../models/training-model';
@Component({
  selector: 'app-payment-data',
  templateUrl: './payment-data.component.html',
  styleUrls: ['./payment-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class PaymentDataComponent {
  @Input() form: FormGroup;
  @Input() trainings: TrainingModel[];
  @Input() mentors: UserModel[];
  constructor() { }

}

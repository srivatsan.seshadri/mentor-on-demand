import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PaymentDataComponent } from './payment-data.component';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';
import { UserModel } from '../../../models/user-model';
import { TrainingModel } from '../../../models/training-model';
import { FormControl, Validators, FormGroup, ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule } from '@angular/material/radio';
import {MatInputModule} from '@angular/material';
import {} from 'jasmine';
describe('PaymentDataComponent', () => {
  let component: PaymentDataComponent;
  let fixture: ComponentFixture<PaymentDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentDataComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [NgxMatDatetimePickerModule, 
                NgxMatTimepickerModule, 
                NgxMatNativeDateModule, 
                ReactiveFormsModule,
                MatSelectModule,
                MatRadioModule,
                MatFormFieldModule,
                MatInputModule,
                BrowserAnimationsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDataComponent);

    component = fixture.componentInstance;
    component.form = new FormGroup({
      id: new FormControl(''),
      mentorId: new FormControl('1'),
      trainingId: new FormControl('1'),
      txnType: new FormControl('1'),
      amount: new FormControl(''),
      paymentDate: new FormControl(''),
      remarks: new FormControl('')
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    component.form = new FormGroup({
      id: new FormControl(''),
      mentorId: new FormControl(''),
      trainingId: new FormControl(''),
      txnType: new FormControl(''),
      amount: new FormControl(''),
      paymentDate: new FormControl(''),
      remarks: new FormControl('')
    });
    var trainings: TrainingModel[];
    var users: UserModel[];
    component.trainings= trainings;
    component.mentors=users;
    expect(component).toBeTruthy();
  });
});

import { NgModule, Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  private appConfig: any;

  constructor(private http: HttpClient) { }

  loadAppConfig() {
    return this.http.get('/assets/development.json')
      .toPromise()
      .then(data => {
        this.appConfig = data;
      });
  }

  get apiUrl() {

    if (!this.appConfig) {
      throw new Error('Config file not loaded!');
    }

    return this.appConfig;
  }
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {} from 'jasmine';
import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { Logger } from '../core/logger/logger.service';

import { TechDetailComponent } from './tech-detail.component';
import { TechnologyService } from '../services/technology.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TechnologyModel } from '../models/technology-model';

describe('TechDetailComponent', () => {
  let component: TechDetailComponent;
  let fixture: ComponentFixture<TechDetailComponent>;
   const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechDetailComponent ],
     schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,RouterTestingModule,HttpClientTestingModule,MatSnackBarModule],
      providers:[TechnologyService,{ provide: AppConfigService, useValue: mockedConfigService},{ provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }]
    })
    .compileComponents();
    configService = TestBed.get(AppConfigService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

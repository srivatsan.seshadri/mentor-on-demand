import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { TechnologyModel } from '../models/technology-model';
import { TechnologyDataComponent } from '../shared/forms/technology-data/technology-data.component';
import { TechnologyService } from '../services/technology.service';

@Component({
  selector: 'app-tech-detail',
  templateUrl: './tech-detail.component.html',
  styleUrls: ['./tech-detail.component.scss']
})
export class TechDetailComponent implements OnInit {
    title = 'Edit Technology';
    form: FormGroup;
  constructor(private techService: TechnologyService,
        private dialogRef: MatDialogRef<TechDetailComponent>,
        @Inject(MAT_DIALOG_DATA) public data: TechnologyModel) { }

  ngOnInit() {
    this.buildForm();
        
  }
  buildForm() {
        this.form = new FormGroup({
            id: new FormControl(this.data.id),
            name: new FormControl(this.data.name, Validators.required),
            termsOfConditions: new FormControl(this.data.termsOfConditions, Validators.required),
            duration: new FormControl(this.data.duration, Validators.required),
            prerequisites: new FormControl(this.data.prerequisites, Validators.required)
        });
    }

    save() {
        if (this.form.invalid) {
            return;
        }
        this.dialogRef.close(this.form.value);
    }

    close() {
        this.dialogRef.close(null);
    }
}

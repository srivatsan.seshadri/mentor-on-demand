import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Logger } from '../core/logger/logger.service';

import { TechnologyModel } from '../models/technology-model';
import { TechnologyService } from '../services/technology.service';
import { TechnologyDataComponent } from '../shared/forms/technology-data/technology-data.component';

@Component({
  selector: 'app-new-tech',
  templateUrl: './new-tech.component.html',
  styleUrls: ['./new-tech.component.scss']
})
export class NewTechComponent implements OnInit {

  title = 'Create Technology';
  techForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private techService: TechnologyService,
    private logger: Logger) { }

  ngOnInit() {
   this. buildTechForm();
  }
  buildTechForm(): void {
    this.techForm = this.formBuilder.group({
      'name': ['', Validators.required],
      'termsOfConditions': ['', Validators.required],
      'duration': ['', Validators.required],
      'prerequisites': ['', Validators.required]
    });
  }

  save() {
    if (this.invalidForms())
      return;

    const newTechnology = this.getTechnologyr();
    this.logger.log(`New Technology: ${newTechnology}`);

    this.techService.addTechnology(newTechnology).subscribe(result => {
      if (result) {
        this.router.navigate(['/technologies']);
      }
    });
  }

  invalidForms(): boolean {
    return (this.techForm.invalid);
  }

  getTechnologyr(): TechnologyModel {
    return this.techForm.value;
  }

}

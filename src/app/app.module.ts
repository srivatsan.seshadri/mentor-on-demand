import { BrowserModule } from '@angular/platform-browser';
import { NgModule,APP_INITIALIZER } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppConfigService } from './config/app-config.module';
import { UserService } from './services/user.service';
import { MentorService } from './services/mentor.service';
import { TechnologyService } from './services/technology.service';
import { TrainingService } from './services/training.service';
import { PaymentService } from './services/payment.service';
import { ProposalService } from './services/proposal.service';
import { SearchService } from './services/search.service';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserListComponent } from './user-list/user-list.component';
import { PaymentListComponent } from './payment-list/payment-list.component';
import { TechnologyListComponent } from './technology-list/technology-list.component';
import { TrainingListComponent } from './training-list/training-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserDataComponent } from './shared/forms/user-data/user-data.component';
import { NewUserComponent } from './new-user/new-user.component';
import { ConfirmedDialogComponent } from './shared/dialogs/confirmed-dialog/confirmed-dialog.component';
import { FormDialogComponent } from './shared/dialogs/form-dialog/form-dialog.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule,MatSelectModule,MatDatepickerModule } from '@angular/material';
import { MatOptionModule } from '@angular/material';
import { TechDetailComponent } from './tech-detail/tech-detail.component';
import { TrainingDetailComponent } from './training-detail/training-detail.component';
import { TechnologyDataComponent } from './shared/forms/technology-data/technology-data.component';
import { NewTechComponent } from './new-tech/new-tech.component';
import { PaymentDetailComponent } from './payment-detail/payment-detail.component';
import { TrainingDataComponent } from './shared/forms/training-data/training-data.component';
import { PaymentDataComponent } from './shared/forms/payment-data/payment-data.component';
import { NewTrainingComponent } from './new-training/new-training.component';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
import { NewPaymentComponent } from './new-payment/new-payment.component';
import { SearchMentorComponent } from './search-mentor/search-mentor.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProposalDataComponent } from './shared/forms/proposal-data/proposal-data.component';
import { ProposalListComponent } from './proposal-list/proposal-list.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    UserListComponent,
    PaymentListComponent,
    TechnologyListComponent,
    TrainingListComponent,
    UserDetailComponent,
    UserDataComponent,
    NewUserComponent,
    ConfirmedDialogComponent,
    TechDetailComponent,
    TrainingDetailComponent,
    TechnologyDataComponent,
    NewTechComponent,
    PaymentDetailComponent,
    TrainingDataComponent,
    PaymentDataComponent,
    NewTrainingComponent,
    FormDialogComponent,
    NewPaymentComponent,
    SearchMentorComponent,
    ProposalDataComponent,
    ProposalListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    MatDialogModule,
    FlexLayoutModule,
    MatOptionModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    NgxMatMomentModule,
    NgbModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [AppConfigService],
      useFactory: (appConfigService: AppConfigService) => {
        return () => {
          return appConfigService.loadAppConfig();
        };
      }
    },
    UserService,
    TechnologyService,
    TrainingService,
    MentorService,
    PaymentService,
    MatDatepickerModule,
    NgxMatDatetimePickerModule,
    ProposalService,
    SearchService
  ],
  exports:[
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatIconModule
  ],
  bootstrap: [AppComponent],
  entryComponents: [UserDetailComponent,TechDetailComponent,TrainingDetailComponent,ConfirmedDialogComponent,FormDialogComponent,PaymentDetailComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { Moment } from 'moment';
import { SearchResultModel } from '../models/search-result-model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, retry, catchError, tap } from 'rxjs/operators';
import { AppConfigService } from '../config/app-config.module';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class SearchService {

  private apiBaseUrl: string;

  constructor(
    private http: HttpClient,
    private logger: Logger,
    private appConfigService: AppConfigService,
    private toastService: ToastService
  ) {
    this.apiBaseUrl = this.appConfigService.apiUrl.searchauthApiEndpoint;
  }

  getSearchResult(id: number, startDate: Date, endDate: Date): Observable<any> {
    const url = this.apiBaseUrl + 'SearchMentorsForTechnologies/' + id + '/' + startDate.toJSON() + '/' + endDate.toJSON();
    return this.http
      .get<SearchResultModel[]>(url)
      .pipe(
      tap(_ => this.notify(`fetched search results for  id=${id} startDate=${startDate} endDate=${endDate}`, 'GET')),
      catchError(this.handleError(`getSearchResult id=${id}`, 'GET'))
      );
  }

  protected handleError(operation: string, method: string) {
    return function errorHandler(res: HttpErrorResponse) {
      this.logger.error(res);
      const eMsg = res.message || '';
      const error = `${this.entityNamePlural} ${operation} Error${
        eMsg ? ': ' + eMsg : ''
        }`;
      this.notify(error, method);
    }.bind(this);
  }

  protected notify(message: string, method: string) {
    this.toastService.openSnackBar(message, method);
  }
}

import {} from 'jasmine';
import { TestBed, inject } from '@angular/core/testing';
import { AppConfigService } from '../config/app-config.module';
import { ProposalService } from './proposal.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ProposalModel } from '../models/proposal-model';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
describe('ProposalService', () => {
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig', 'apiUrl']);
  let configService: AppConfigService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProposalService,ToastService,Logger, { provide: AppConfigService, useValue: mockedConfigService }],
      imports: [HttpClientTestingModule,MatSnackBarModule]
    });
  });

  it('should be created', inject([ProposalService], (service: ProposalService) => {
    expect(service).toBeTruthy();
  }));
});
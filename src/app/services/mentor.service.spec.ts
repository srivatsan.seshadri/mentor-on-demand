import { TestBed, inject } from '@angular/core/testing';
import { AppConfigService } from '../config/app-config.module';
import { MentorService } from './mentor.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MentorModel } from '../models/mentor-model';
import { UserModel } from '../models/user-model';
import { ToastService } from '../core/toast/toast.service';
import { Logger } from '../core/logger/logger.service';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import {} from 'jasmine';
describe('MentorService', () => {
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig', 'apiUrl']);
  let configService: AppConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MentorService,ToastService,Logger, { provide: AppConfigService, useValue: mockedConfigService }],
      imports: [HttpClientTestingModule,MatSnackBarModule]
    });
  });

  it('should be created', inject([MentorService], (service: MentorService) => {
    expect(service).toBeTruthy();
  }));
});
import {} from 'jasmine';
import { TestBed, inject } from '@angular/core/testing';
import { AppConfigService } from '../config/app-config.module';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { SearchService } from './search.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SearchResultModel } from '../models/search-result-model';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';
describe('SearchService', () => {
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig', 'apiUrl']);
  let configService: AppConfigService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchService,ToastService,Logger, { provide: AppConfigService, useValue: mockedConfigService }],
      imports: [HttpClientTestingModule,MatSnackBarModule]
    });
  });

  it('should be created', inject([SearchService], (service: SearchService) => {
    expect(service).toBeTruthy();
  }));
});
import { Injectable } from '@angular/core';
import { PaymentModel } from '../models/payment-model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, retry, catchError, tap } from 'rxjs/operators';
import { AppConfigService } from '../config/app-config.module';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PaymentService {
    private tech: PaymentModel = new PaymentModel();
    private apiBaseUrl: string;

    constructor(
        private http: HttpClient,
        private logger: Logger,
        private appConfigService: AppConfigService,
        private toastService: ToastService
    ) {
        this.apiBaseUrl = this.appConfigService.apiUrl.payApiEndpoint;
    }

    getPayments(): Observable<any> {
        const url = this.apiBaseUrl + 'GetAllPayments'
        this.logger.log(url);
        return this.http
            .get<PaymentModel[]>(url)
            .pipe(
            tap(_ => this.notify('fetched payments', 'GET')),
            catchError(this.handleError('getPayments', 'GET'))
            );
    }
    getPaymentsList(): Observable<any> {
        const url = this.apiBaseUrl + 'GetAllPaymentsForList'
        this.logger.log(url);
        return this.http
            .get<PaymentModel[]>(url)
            .pipe(
            tap(_ => this.notify('fetched payments', 'GET')),
            catchError(this.handleError('getPaymentsList', 'GET'))
            );
    }

    getPayment(id: number): Observable<any> {
        const url = this.apiBaseUrl + 'GetPaymentById/' + id;
        return this.http
            .get<PaymentModel>(url)
            .pipe(
            tap(_ => this.notify(`fetched payment id=${id}`, 'GET')),
            catchError(this.handleError(`getPayment id=${id}`, 'GET'))
            );
    }

    addPayment(payment: PaymentModel): Observable<any> {
        const url = this.apiBaseUrl + 'AddPayment'
        var paymentRequest = { payment: payment };
        return this.http
            .post<PaymentModel>(url, paymentRequest, httpOptions)
            .pipe(
            tap((user: PaymentModel) => this.notify(`added payment w/ id=${user.id}`, 'POST')),
            catchError(this.handleError('addPayment', 'POST'))
            );
    }

    updatePayment(payment: PaymentModel): Observable<any> {
        const url = this.apiBaseUrl + 'UpdatePayment'
        var paymentRequest = { payment: payment };
        return this.http
            .put(url, paymentRequest, httpOptions)
            .pipe(
            tap(_ => this.notify(`updated payment id=${payment.id}`, 'PUT')),
            catchError(this.handleError('updatePayment', 'PUT'))
            );
    }
    deletePayment(payment: PaymentModel): Observable<any> {
        var paymentRequest = { payment: payment };
        const url = this.apiBaseUrl + 'DeletePayment';
        return this.http
            .post<PaymentModel>(url, paymentRequest, httpOptions)
            .pipe(
            tap(_ => this.notify(`deleted payment id=${payment.id}`, 'DELETE')),
            catchError(this.handleError('deletePayment', 'DELETE'))
            );
    }

    protected handleError(operation: string, method: string) {
        return function errorHandler(res: HttpErrorResponse) {
            this.logger.error(res);
            const eMsg = res.message || '';
            const error = `${this.entityNamePlural} ${operation} Error${
                eMsg ? ': ' + eMsg : ''
                }`;
            this.notify(error, method);
        }.bind(this);
    }

    protected notify(message: string, method: string) {
        this.toastService.openSnackBar(message, method);
    }
}

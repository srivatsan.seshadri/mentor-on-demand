import { TestBed, inject } from '@angular/core/testing';
import { AppConfigService } from '../config/app-config.module';
import { PaymentService } from './payment.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PaymentModel } from '../models/payment-model';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import {} from 'jasmine';
describe('PaymentService', () => {
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig', 'apiUrl']);
  let configService: AppConfigService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PaymentService,ToastService,Logger, { provide: AppConfigService, useValue: mockedConfigService }],
      imports: [HttpClientTestingModule,MatSnackBarModule]
    });
  });

  it('should be created', inject([PaymentService], (service: PaymentService) => {
    expect(service).toBeTruthy();
  }));
});
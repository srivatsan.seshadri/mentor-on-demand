import { Injectable } from '@angular/core';
import { MentorModel } from '../models/mentor-model';
import { UserModel } from '../models/user-model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, retry, catchError, tap } from 'rxjs/operators';
import { AppConfigService } from '../config/app-config.module';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class MentorService {
  private apiBaseUrl: string;

  constructor(
    private http: HttpClient,
    private logger: Logger,
    private appConfigService: AppConfigService,
    private toastService: ToastService
  ) {
    this.apiBaseUrl = this.appConfigService.apiUrl.userauthApiEndpoint;
  }

  getMentors(): Observable<any> {
    const url = this.apiBaseUrl + 'GetAllMentors'
    this.logger.log(url);
    return this.http
      .get<MentorModel[]>(url)
      .pipe(
      tap(_ => this.notify('fetched mentors', 'GET')),
      catchError(this.handleError('getMentors', 'GET'))
      );
  }
  getUser(id: number): Observable<any> {
    const url = this.apiBaseUrl + 'GetUserById/' + id;
    return this.http
      .get<UserModel>(url)
      .pipe(
      tap(_ => this.notify(`fetched user id=${id}`, 'GET')),
      catchError(this.handleError(`getUser id=${id}`, 'GET'))
      );
  }
  protected handleError(operation: string, method: string) {
    return function errorHandler(res: HttpErrorResponse) {
      this.logger.error(res);
      const eMsg = res.message || '';
      const error = `${this.entityNamePlural} ${operation} Error${
        eMsg ? ': ' + eMsg : ''
        }`;
      this.notify(error, method);
    }.bind(this);
  }

  protected notify(message: string, method: string) {
    this.toastService.openSnackBar(message, method);
  }
}

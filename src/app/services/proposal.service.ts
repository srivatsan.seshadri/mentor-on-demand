import { Injectable } from '@angular/core';
import { ProposalModel } from '../models/proposal-model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, retry, catchError, tap } from 'rxjs/operators';
import { AppConfigService } from '../config/app-config.module';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ProposalService {

    private proposal: ProposalModel = new ProposalModel();
    private apiBaseUrl: string;

  constructor(
        private http: HttpClient,
        private logger: Logger,
        private appConfigService: AppConfigService,
        private toastService: ToastService
    ) {
        this.apiBaseUrl = this.appConfigService.apiUrl.searchauthApiEndpoint;
    }

    getProposals(): Observable<any> {
        const url = this.apiBaseUrl + 'GetAllProposals'
        this.logger.log(url);
        return this.http
            .get<ProposalModel[]>(url)
            .pipe(
            tap(_ => this.notify('fetched proposals', 'GET')),
            catchError(this.handleError('getProposals', 'GET'))
            );
    }
    
    getProposal(id: number): Observable<any> {
        const url = this.apiBaseUrl + 'GetProposalsById/' + id;
        return this.http
            .get<ProposalModel>(url)
            .pipe(
            tap(_ => this.notify(`fetched proposal id=${id}`, 'GET')),
            catchError(this.handleError(`getProposal id=${id}`, 'GET'))
            );
    }

    addProposal(proposal: ProposalModel): Observable<any> {
        const url = this.apiBaseUrl + 'AddProposal'
        var proposalRequest = { proposal: proposal };
        return this.http
            .post<ProposalModel>(url, proposalRequest, httpOptions)
            .pipe(
            tap((proposal: ProposalModel) => this.notify(`added proposal w/ id=${proposal.id}`, 'POST')),
            catchError(this.handleError('addProposal', 'POST'))
            );
    }

    updateProposal(proposal: ProposalModel): Observable<any> {
        const url = this.apiBaseUrl + 'UpdateProposal'
        var proposalRequest = { proposal: proposal };
        return this.http
            .put(url, proposalRequest, httpOptions)
            .pipe(
            tap(_ => this.notify(`updated proposal id=${proposal.id}`, 'PUT')),
            catchError(this.handleError('updateProposal', 'PUT'))
            );
    }
    deleteProposal(proposal: ProposalModel): Observable<any> {
        var proposalRequest = { proposal: proposal };
        const url = this.apiBaseUrl + 'DeleteProposal';
        return this.http
            .post<ProposalModel>(url, proposalRequest, httpOptions)
            .pipe(
            tap(_ => this.notify(`deleted payment id=${proposal.id}`, 'DELETE')),
            catchError(this.handleError('deleteProposal', 'DELETE'))
            );
    }

    protected handleError(operation: string, method: string) {
        return function errorHandler(res: HttpErrorResponse) {
            this.logger.error(res);
            const eMsg = res.message || '';
            const error = `${this.entityNamePlural} ${operation} Error${
                eMsg ? ': ' + eMsg : ''
                }`;
            this.notify(error, method);
        }.bind(this);
    }

    protected notify(message: string, method: string) {
        this.toastService.openSnackBar(message, method);
    }
}

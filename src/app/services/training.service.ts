import { Injectable } from '@angular/core';
import { TrainingModel } from '../models/training-model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, retry, catchError, tap } from 'rxjs/operators';
import { AppConfigService } from '../config/app-config.module';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class TrainingService {

  private training: TrainingModel = new TrainingModel();
  private apiBaseUrl: string;

  constructor(
    private http: HttpClient,
    private logger: Logger,
    private appConfigService: AppConfigService,
    private toastService: ToastService
  ) {
    this.apiBaseUrl = this.appConfigService.apiUrl.trainingApiEndpoint;
  }

  getTrainings(): Observable<any> {
    const url = this.apiBaseUrl + 'GetAllTrainings'
    this.logger.log(url);
    return this.http
      .get<TrainingModel[]>(url)
      .pipe(
      tap(_ => this.notify('fetched trainings', 'GET')),
      catchError(this.handleError('getTrainings', 'GET'))
      );
  }

    getTrainingsForList(): Observable<any> {
    const url = this.apiBaseUrl + 'GetAllTrainingsForList'
    this.logger.log(url);
    return this.http
      .get<TrainingModel[]>(url)
      .pipe(
      tap(_ => this.notify('fetched trainings', 'GET')),
      catchError(this.handleError('getTrainings', 'GET'))
      );
  }

  getTraining(id: number): Observable<any> {
    const url = this.apiBaseUrl + 'GetTrainingById/' + id;
    return this.http
      .get<TrainingModel>(url)
      .pipe(
      tap(_ => this.notify(`fetched training id=${id}`, 'GET')),
      catchError(this.handleError(`getTraining id=${id}`, 'GET'))
      );
  }

  addTraining(training: TrainingModel): Observable<any> {
    const url = this.apiBaseUrl + 'AddTraining'
    var trainingRequest = { training: training };
    return this.http
      .post<TrainingModel>(url, trainingRequest, httpOptions)
      .pipe(
      tap((training: TrainingModel) => this.notify(`added training w/ id=${training.id}`, 'POST')),
      catchError(this.handleError('addTraining', 'POST'))
      );
  }

  updateTraining(training: TrainingModel): Observable<any> {
    const url = this.apiBaseUrl + 'UpdateTraining'
    var trainingRequest = { training: training };
    return this.http
      .put(url, trainingRequest, httpOptions)
      .pipe(
      tap(_ => this.notify(`updated trainng id=${training.id}`, 'PUT')),
      catchError(this.handleError('updateTraining', 'PUT'))
      );
  }
  deleteTraining(training: TrainingModel): Observable<any> {
    var trainingRequest = { training: training };
    const url = this.apiBaseUrl + 'DeleteTraining';
    return this.http
      .post<TrainingModel>(url, trainingRequest, httpOptions)
      .pipe(
      tap(_ => this.notify(`deleted training id=${training.id}`, 'DELETE')),
      catchError(this.handleError('deletePayment', 'DELETE'))
      );
  }

  protected handleError(operation: string, method: string) {
    return function errorHandler(res: HttpErrorResponse) {
      this.logger.error(res);
      const eMsg = res.message || '';
      const error = `${this.entityNamePlural} ${operation} Error${
        eMsg ? ': ' + eMsg : ''
        }`;
      this.notify(error, method);
    }.bind(this);
  }

  protected notify(message: string, method: string) {
    this.toastService.openSnackBar(message, method);
  }
}

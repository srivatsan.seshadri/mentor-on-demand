import { Injectable } from '@angular/core';
import { TechnologyModel } from '../models/technology-model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, retry, catchError, tap } from 'rxjs/operators';
import { AppConfigService } from '../config/app-config.module';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class TechnologyService {

 private tech: TechnologyModel = new TechnologyModel();
    private apiBaseUrl: string;

    constructor(
        private http: HttpClient,
        private logger: Logger,
        private appConfigService: AppConfigService,
        private toastService: ToastService
    ) {
        this.apiBaseUrl = this.appConfigService.apiUrl.techApiEndpoint;
    }

    getTechnologies(): Observable<any> {
        const url = this.apiBaseUrl + 'GetAllTechnologies'
        this.logger.log(url);
        return this.http
            .get<TechnologyModel[]>(url)
            .pipe(
            tap(_ => this.notify('fetched technologies', 'GET')),
            catchError(this.handleError('getTechnologies', 'GET'))
            );
    }

    getTechnology(id: number): Observable<any> {
        const url = this.apiBaseUrl + 'GetTechnologyById/' + id;
        return this.http
            .get<TechnologyModel>(url)
            .pipe(
            tap(_ => this.notify(`fetched technology id=${id}`, 'GET')),
            catchError(this.handleError(`getTechnology id=${id}`, 'GET'))
            );
    }

    addTechnology(tech: TechnologyModel): Observable<any> {
        const url = this.apiBaseUrl + 'AddTechnology'
        var techRequest = { technology: tech };
        return this.http
            .post<TechnologyModel>(url, techRequest, httpOptions)
            .pipe(
            tap((user: TechnologyModel) => this.notify(`added technology w/ id=${user.id}`, 'POST')),
            catchError(this.handleError('addTechnology', 'POST'))
            );
    }

    updateTechnology(tech: TechnologyModel): Observable<any> {
        const url = this.apiBaseUrl + 'UpdateTechnology'
        var techRequest = { technology: tech };
        return this.http
            .put(url, techRequest, httpOptions)
            .pipe(
            tap(_ => this.notify(`updated technology id=${tech.id}`, 'PUT')),
            catchError(this.handleError('updateTechnology', 'PUT'))
            );
    }
    deleteTechnology(tech: TechnologyModel): Observable<any> {
        var techRequest = { technology: tech };
        const url = this.apiBaseUrl + 'DeleteTechnology';
        return this.http
            .post<TechnologyModel>(url, techRequest, httpOptions)
            .pipe(
            tap(_ => this.notify(`deleted technology id=${tech.id}`, 'DELETE')),
            catchError(this.handleError('deleteTechnology', 'DELETE'))
            );
    }

    protected handleError(operation: string, method: string) {
        return function errorHandler(res: HttpErrorResponse) {
            this.logger.error(res);
            const eMsg = res.message || '';
            const error = `${this.entityNamePlural} ${operation} Error${
                eMsg ? ': ' + eMsg : ''
                }`;
            this.notify(error, method);
        }.bind(this);
    }

    protected notify(message: string, method: string) {
        this.toastService.openSnackBar(message, method);
    }
}

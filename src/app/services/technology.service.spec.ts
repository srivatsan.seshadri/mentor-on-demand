import {} from 'jasmine';
import { TestBed, inject } from '@angular/core/testing';
import { AppConfigService } from '../config/app-config.module';
import { TechnologyService } from './technology.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TechnologyModel } from '../models/technology-model';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
describe('TechnologyService', () => {
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig', 'apiUrl']);
  let configService: AppConfigService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TechnologyService,ToastService,Logger, { provide: AppConfigService, useValue: mockedConfigService }],
      imports: [HttpClientTestingModule,MatSnackBarModule]
    });
  });

  it('should be created', inject([TechnologyService], (service: TechnologyService) => {
    expect(service).toBeTruthy();
  }));
});
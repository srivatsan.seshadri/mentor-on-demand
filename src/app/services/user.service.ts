import { Injectable } from '@angular/core';
import { UserModel } from '../models/user-model';
import { RoleModel } from '../models/role-model';
import { MentorSkillsModel } from '../models/mentor-skills-model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, retry, catchError, tap } from 'rxjs/operators';
import { AppConfigService } from '../config/app-config.module';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

    private user: UserModel = new UserModel();
    private apiBaseUrl: string;

    constructor(
        private http: HttpClient,
        private logger: Logger,
        private appConfigService: AppConfigService,
        private toastService: ToastService
    ) {
        this.apiBaseUrl = this.appConfigService.apiUrl.userauthApiEndpoint;
    }

    getUsers(): Observable<any> {
        const url = this.apiBaseUrl + 'GetAllUsers'
        this.logger.log(url);
        return this.http
            .get<UserModel[]>(url)
            .pipe(
            tap(_ => this.notify('fetched users', 'GET')),
            catchError(this.handleError('getUsers', 'GET'))
            );
    }

    getUser(id: number): Observable<any> {
        const url = this.apiBaseUrl + 'GetUserById/' + id;
        return this.http
            .get<UserModel>(url)
            .pipe(
            tap(_ => this.notify(`fetched user id=${id}`, 'GET')),
            catchError(this.handleError(`getUser id=${id}`, 'GET'))
            );
    }

    addUser(user: UserModel): Observable<any> {
        const url = this.apiBaseUrl + 'AddUser'
        var userRequest = { user: user };
        return this.http
            .post<UserModel>(url, userRequest, httpOptions)
            .pipe(
            tap((user: UserModel) => this.notify(`added user w/ id=${user.id}`, 'POST')),
            catchError(this.handleError('addUser', 'POST'))
            );
    }

    updateUser(user: UserModel): Observable<any> {
        const url = this.apiBaseUrl + 'UpdateUser'
        var userRequest = { user: user };
        return this.http
            .put(url, userRequest, httpOptions)
            .pipe(
            tap(_ => this.notify(`updated user id=${user.id}`, 'PUT')),
            catchError(this.handleError('updateUser', 'PUT'))
            );
    }
    deleteUser(user: UserModel): Observable<any> {
        var userRequest = { user: user };
        const url = this.apiBaseUrl + 'DeleteUser/';
        return this.http
            .post<UserModel>(url, userRequest, httpOptions)
            .pipe(
            tap(_ => this.notify(`deleted user id=${user.id}`, 'DELETE')),
            catchError(this.handleError('deleteUser', 'DELETE'))
            );
    }


    getRoles(): Observable<any> {
        const url = this.apiBaseUrl + 'GetAllRoles'
        this.logger.log(url);
        return this.http
            .get<RoleModel[]>(url)
            .pipe(
            tap(_ => this.notify('fetched roles', 'GET')),
            catchError(this.handleError('getRoles', 'GET'))
            );
    }
    
    getMentorSkills(id): Observable<any> {
        const url = this.apiBaseUrl + 'GetMentorSkillsById/' + id;
        this.logger.log(url);
        return this.http
            .get<MentorSkillsModel[]>(url)
            .pipe(
            tap(_ => this.notify('fetched roles', 'GET')),
            catchError(this.handleError('getRoles', 'GET'))
            );
    }

    protected handleError(operation: string, method: string) {
        return function errorHandler(res: HttpErrorResponse) {
            this.logger.error(res);
            const eMsg = res.message || '';
            const error = `${this.entityNamePlural} ${operation} Error${
                eMsg ? ': ' + eMsg : ''
                }`;
            this.notify(error, method);
        }.bind(this);
    }

    protected notify(message: string, method: string) {
        this.toastService.openSnackBar(message, method);
    }
}

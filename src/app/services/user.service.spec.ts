import {} from 'jasmine';
import { TestBed, inject } from '@angular/core/testing';
import { AppConfigService } from '../config/app-config.module';
import { UserService } from './user.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TrainingModel } from '../models/training-model';
import { Logger } from '../core/logger/logger.service';
import { ToastService } from '../core/toast/toast.service';
import { UserModel } from '../models/user-model';
import { RoleModel } from '../models/role-model';
import { MentorSkillsModel } from '../models/mentor-skills-model';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
describe('UserService', () => {
  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig', 'apiUrl']);
  let configService: AppConfigService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService,ToastService,Logger, { provide: AppConfigService, useValue: mockedConfigService }],
      imports: [HttpClientTestingModule,MatSnackBarModule]
    });
  });

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));
});
import {} from 'jasmine';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppConfigService } from '../config/app-config.module';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';

import { TrainingListComponent } from './training-list.component';

import {NavbarComponent} from '../navbar/navbar.component'
import { Component, HostListener, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatDialogModule} from '@angular/material';
import { MatTableModule ,MatTableDataSource } from '@angular/material';
import { ConfirmedDialogComponent } from '../shared/dialogs/confirmed-dialog/confirmed-dialog.component';
import { Logger } from '../core/logger/logger.service';
import { TrainingModel } from '../models/training-model';
import { TrainingListModel } from '../models/training-list-model';
import { TechnologyModel } from '../models/technology-model';
import { MentorModel } from '../models/mentor-model';
import { TrainingDetailComponent } from '../training-detail/training-detail.component';
import { TrainingService } from '../services/training.service';
import { TechnologyService } from '../services/technology.service';
import { UserModel } from '../models/user-model';
import { UserService } from '../services/user.service';
import { MentorService } from '../services/mentor.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('TrainingListComponent', () => {
  let component: TrainingListComponent;
  let fixture: ComponentFixture<TrainingListComponent>;

  const mockedConfigService: any = jasmine.createSpyObj('AppConfigService', ['loadAppConfig','apiUrl']);
  let configService: AppConfigService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingListComponent,NavbarComponent,ConfirmedDialogComponent,TrainingDetailComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports:[ReactiveFormsModule,
               RouterTestingModule,
               HttpClientTestingModule,
               MatSnackBarModule,
               MatPaginatorModule,
               MatSortModule,
               MatTableModule,
               MatDialogModule,
               BrowserAnimationsModule],
      providers:[TrainingService,UserService,MentorService,TechnologyService,{ provide: AppConfigService, useValue: mockedConfigService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, HostListener, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { MatTableDataSource } from '@angular/material';
import { ConfirmedDialogComponent } from '../shared/dialogs/confirmed-dialog/confirmed-dialog.component';
import { Logger } from '../core/logger/logger.service';
import { TrainingModel } from '../models/training-model';
import { TrainingListModel } from '../models/training-list-model';
import { TechnologyModel } from '../models/technology-model';
import { MentorModel } from '../models/mentor-model';
import { TrainingDetailComponent } from '../training-detail/training-detail.component';
import { TrainingService } from '../services/training.service';
import { TechnologyService } from '../services/technology.service';
import { UserModel } from '../models/user-model';
import { UserService } from '../services/user.service';
import { MentorService } from '../services/mentor.service';

@Component({
  selector: 'app-training-list',
  templateUrl: './training-list.component.html',
  styleUrls: ['./training-list.component.scss']
})

export class TrainingListComponent implements OnInit {

  displayedColumns: string[];
  dataSource = new MatTableDataSource();
  screenHeight: any;
  screenWidth: any;
  technologies: TechnologyModel[];
  mentors: MentorModel[];

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  @HostListener('window:resize', ['$event'])

  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    this.logger.log(`Resize() height: ${this.screenHeight}; width: ${this.screenWidth}`);
    this.setDisplayedColumns();
  }

  constructor(
    private trainingService: TrainingService,
    private techService: TechnologyService,
    private userService: UserService,
    private mentorService: MentorService,
    private logger: Logger,
    private dialog: MatDialog,
    private cdr: ChangeDetectorRef) {

    this.screenHeight = window.screen.height;
    this.screenWidth = window.screen.width;
    this.logger.log(`Init() height: ${this.screenHeight}; width: ${this.screenWidth}`);
    this.setDisplayedColumns();
  }

  ngOnInit() {
    this.loadTrainings();
    this.cdr.detectChanges();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  loadTrainings() {
    this.trainingService.getTrainingsForList().subscribe(data => {
      this.dataSource.data = data;
    });
  }

  editTraining(id: number): void {

    this.trainingService.getTraining(id).subscribe(data => {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.height = '400px';
      dialogConfig.width = '500px';
      dialogConfig.data = data;

      const dialogRef = this.dialog.open(TrainingDetailComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (!result) {
          return;
        }
        this.trainingService.updateTraining(result)
          .subscribe(_ => this.loadTrainings());
      });
    });
  }

  deleteTraining(training: TrainingListModel): void {
    // Create configuration for the dialog
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.height = '200px';
    dialogConfig.width = '400px';
    dialogConfig.data = {
      title: `Delete  ${training.id}`,
      message: 'Are you sure to delete this Training?'
    };

    const dialogRef = this.dialog.open(ConfirmedDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dataSource.data = this.dataSource.data.filter(e => e != training);
        this.trainingService.getTraining(training.id).subscribe(training => {
          this.trainingService.deleteTraining(training).subscribe();
        });
      }
    });
  }

  setDisplayedColumns() {
    if (this.screenWidth < 420) {
      this.displayedColumns = ['mentorName', 'userName', 'progress', 'status', 'skill', 'fees', 'startDate', 'endDate', 'action'];
    }
    else if (this.screenWidth >= 420 && this.screenWidth <= 800) {
      this.displayedColumns = ['mentorName', 'progress', 'status', 'skill', 'fees', 'action'];
    }
    else {
      this.displayedColumns = ['mentorName', 'progress', 'status', 'skill', 'fees', 'startDate', 'endDate', 'action'];
    }
  }

  getFullName(mentor: MentorModel): string {
    return `${mentor.firstName} ${mentor.lastName}`;
  }

  getUserName(user: UserModel): string {
    return `${user.firstName} ${user.lastName}`;
  }
  getTrainingSkill(skill: TechnologyModel): string {
    return skill.name;
  }
}
